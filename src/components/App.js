import React, { Component } from 'react'
import QuestsionList from './quiz/QuestionList'
import ScoreBox from './quiz/ScoreBox'
import Results from './quiz/Results'

class App extends Component {
    constructor(props) {
        super(props)

        this.state = {
            questions: [
                {
                    id: 1,
                    text: 'What is u name?',
                    choises: [
                        {
                            id: 'a',
                            text: 'Michael'
                        }, {
                            id: 'b',
                            text: 'Marta'
                        }, {
                            id: 'c',
                            text: 'Marus'
                        }                        
                    ],
                    correct: 'b'
                }, {
                    id: 2,
                    text: 'What is u surename?',
                    choises: [
                        {
                            id: 'a',
                            text: 'Swift'
                        }, {
                            id: 'b',
                            text: 'Johnes'
                        }, {
                            id: 'c',
                            text: 'McQueen'
                        }                        
                    ],
                    correct: 'c'
                }, {
                    id: 3,
                    text: 'What is u pet`s name?',
                    choises: [
                        {
                            id: 'a',
                            text: 'Doggy'
                        }, {
                            id: 'b',
                            text: 'Bibby'
                        }, {
                            id: 'c',
                            text: 'Sissy'
                        }                        
                    ],
                    correct: 'b'
                }
            ],
            score: 0,
            current: 1
        }
    }

     setCurrent(current) {
        this.setState({
            current: current
        });
    }

    setScore(score) {
        this.setState({
            score: score
        });
    }

    render() {
         let scorebox = null;
         let results = null; 
         
         if (this.state.current > this.state.questions.length) {
            results = <Results {...this.state} />;
         } else {            
            scorebox = <ScoreBox {...this.state} />;
         }
         return (
            <div>
                {results}
                {scorebox}
                <QuestsionList {...this.state} 
                    setCurrent={this.setCurrent.bind(this)} 
                    setScore={this.setScore.bind(this)} />
            </div>
        );
    }
}

export default App;