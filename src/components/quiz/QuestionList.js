import React, { Component } from 'react'
import Question from './Question'

class QuestionList extends Component {
    render() {
        const {questions, current} = this.props;
        
        return (
            <div>
                {questions.map((question, index) => {
                    if (question.id === current) {
                        return <Question 
                            question={question}
                            key={question.id} 
                            {...this.props} />
                    }                    
                })}
            </div>
        );
    }
}

export default QuestionList;