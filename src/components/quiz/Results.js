import React from 'react'

const Results = props => {
    const {questions, score} = props;
    const persent = score / questions.length * 100;

    return (
        <div>
            <p>You got {score} out of {questions.length} correct answers</p>
            <h2>{persent.toFixed(0)}%</h2>
            <hr />
            <a href="/app">Take it again</a>
        </div>
    )
}

export default Results;