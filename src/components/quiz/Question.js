import React, { Component } from 'react'

class Question extends Component {

    onChange(ev) {
        ev.preventDefault();

        const {setCurrent, setScore, question} = this.props;

        let selected = ev.target.value;

        if (selected === question.correct) {
            setScore(this.props.score + 1);
        }
        
        setCurrent(this.props.current + 1);
    }

    render() {
        const {question} = this.props;

        return (
            <div className='well'>
                <h3>{question.text}</h3>
                
                <ul className='list-group'>
                    {question.choises.map( (choise) => {
                        return (
                            <li className='list-group-item' key={choise.id}>
                                {choise.id}{'  '}
                                <label>
                                    <input type='radio' 
                                        onChange={this.onChange.bind(this)} 
                                        name={question.id} 
                                        value={choise.id} />{'  '}
                                {choise.text}</label>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

export default Question;