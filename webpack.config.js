module.exports = {
    entry: [
        './src/index.js'
    ], 
    output: {
        path: __dirname,
        filename: 'app/js/app.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel',
                exclude: /node_modules/
            }
        ]
    },
    devServer: {
        contentBase: "./app",
        hot: true
    }
}